package com.gestionnairedenote.gestionnairedenote.testunitaire;

import com.gestionnairedenote.gestionnairedenote.model.Notes;
import com.gestionnairedenote.gestionnairedenote.services.Calcul;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CalculTest{

    @Test
    void calculMoyenne() {
       List<Notes> notes = new ArrayList(Arrays.asList(new Notes(10),new Notes(6)));

        ArrayList doubles = new ArrayList();

        // test valeur juste
        Calcul calcul = new Calcul();
        double moyenne = calcul.calculMoyenne( notes, doubles);
        assertEquals(8.0, moyenne);
        assertEquals(doubles.size(),2);


        //test valeur avec virgule
        notes.add(new Notes(12.5));
        assertEquals(9.5,calcul.calculMoyenne(notes,doubles));

        notes.add(new Notes(15.5));
        assertEquals(11,calcul.calculMoyenne(notes,doubles));

        //valeur negative
        notes.add(new Notes(-2));
        assertEquals(8.4,calcul.calculMoyenne(notes,doubles));

    }


}


