package com.gestionnairedenote.gestionnairedenote.testfonctionnel;

import com.gestionnairedenote.gestionnairedenote.GestionnaireDeNoteApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest(classes = GestionnaireDeNoteApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class testApi {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    //recup la note 1
    @Test
    public void note1() {
        assertEquals(11.0, this.restTemplate.getForObject("http://localhost:" + port + "/Note/1", Double.class),
                "c pa bo");
    }

    //recup la note 2
    @Test
    public void note2() {
        assertEquals(14.0, this.restTemplate.getForObject("http://localhost:" + port + "/Note/2", Double.class),
                "ciii pa bo");
    }

    //recup la moyenne general etudiant 1
    @Test
    public void moyenneGeneralEtudiant1() {

        assertEquals(13.5, this.restTemplate.getForObject("http://localhost:" + port + "/Note/moyenneG/1", Double.class),
                "ciii bo");
    }

    //recup la moyenne matiere 1 etudiant 1
    @Test
    public void moyenneMatiere1Etudiant1() {

        assertEquals(12.5, this.restTemplate.getForObject("http://localhost:" + port + "/Note/moyenneM/1/1", Double.class),
                "ciii bo");
    }

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnHichem() throws Exception {
        this.mockMvc.perform(get("/Student/1")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("hichem")));
    }

}