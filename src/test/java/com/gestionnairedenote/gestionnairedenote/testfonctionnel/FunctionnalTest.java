package com.gestionnairedenote.gestionnairedenote.testfonctionnel;

import com.gestionnairedenote.gestionnairedenote.coucheMetier.GeneralMoyenneMetier;
import com.gestionnairedenote.gestionnairedenote.coucheMetier.MatiereMoyenneMetier;
import com.gestionnairedenote.gestionnairedenote.persistance.NotesDao;
import com.gestionnairedenote.gestionnairedenote.persistance.StudentsDao;
import com.gestionnairedenote.gestionnairedenote.presentation.controller.NotesController;
import com.gestionnairedenote.gestionnairedenote.presentation.controller.StudentController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


//@RunWith(SpringRunner.class)
//@DataJpaTest
@SpringBootTest
public class FunctionnalTest {
    @Autowired
    private NotesDao notesDao;
    @Autowired
    private StudentsDao studentDao;

    //userstory1 connaitre la moyenne d'un eleve dans une matiere donnéé
    @Test
    public void moyenneEleve1ParMatiere1 (){


        NotesController notesController = new NotesController(notesDao);

        MatiereMoyenneMetier Notes = notesController.afficherlesNoteStudentParMatiereetSaMoyenneM(1, 1);
        assertEquals(12.5,Notes.getMoyenne());
        assertEquals(2,Notes.getNotes1().size());

    }

    @Test
    public void moyenneEleve2ParMatiere1 (){


        NotesController notesController = new NotesController(notesDao);

        MatiereMoyenneMetier Notes = notesController.afficherlesNoteStudentParMatiereetSaMoyenneM(1, 2);
        assertEquals(8,Notes.getMoyenne());
        assertEquals(2,Notes.getNotes1().size());

    }


    @Test
    public void moyenneEleve1ParMatiere2 (){


        NotesController notesController = new NotesController(notesDao);

        MatiereMoyenneMetier Notes = notesController.afficherlesNoteStudentParMatiereetSaMoyenneM(2, 1);
        assertEquals(14.5,Notes.getMoyenne());
        assertEquals(2,Notes.getNotes1().size());

    }

    @Test
    public void moyenneEleve2ParMatiere2 (){


        NotesController notesController = new NotesController(notesDao);

        MatiereMoyenneMetier Notes = notesController.afficherlesNoteStudentParMatiereetSaMoyenneM(2, 2);
        assertEquals(8.5,Notes.getMoyenne());
        assertEquals(2,Notes.getNotes1().size());

    }

    @Test
    public void moyenneGeneralEleve () {
        NotesController notesController = new NotesController(notesDao);

        GeneralMoyenneMetier Notes = notesController.affichetoutesNotesDestudentEtSaMoyenneG(1);
        assertEquals(13.5,Notes.getMoyenne());
        assertEquals(4,Notes.getNotes().size());
    }


    @Test
    public void notesDuneMatiere () {

        NotesController notesController1 = new NotesController(notesDao);

        List<Double> Notes = notesController1.afficherLesNoteparMatiere(1);
        assertEquals(8,Notes.size());

    }

    @Test
    public void shouldReturnHichem() {

        StudentController studentController = new StudentController(studentDao);
        String students = studentController.afficherUnStudent(1);
        assertEquals("hichem",students);
    }


    @Test
    public void shouldReturnFatima() {

        StudentController studentController = new StudentController(studentDao);
        String students = studentController.afficherUnStudent(2);
        assertEquals("fatima",students);
    }



}
