package com.gestionnairedenote.gestionnairedenote.services;

import com.gestionnairedenote.gestionnairedenote.model.Notes;

import java.util.List;

public class Calcul {
// je passe deux arguments pour recuperer la liste des notes et leur moyenne pour eviter la repition du traitement sur la meme route
    public double calculMoyenne(List<Notes> notes,List<Double> notes1) {

            double somme = 0;

            for (Notes number: notes) {

            somme += number.getValeur();
            notes1.add(number.getValeur());
            }
            return somme / notes.toArray().length;

            }
        }