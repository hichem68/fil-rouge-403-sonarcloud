package com.gestionnairedenote.gestionnairedenote.coucheMetier;

public class StudentMetier {
    private String nom;
    private double moyenne;

    public StudentMetier() {}

    public StudentMetier(String nom, double moyenne) {
        this.nom = nom;
        this.moyenne  = moyenne;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getMoyenne() {
        return moyenne;
    }

    public void setMoyenne(double moyenne) {
        this.moyenne = moyenne;
    }
}
