package com.gestionnairedenote.gestionnairedenote.coucheMetier;

import com.gestionnairedenote.gestionnairedenote.model.Notes;
import com.gestionnairedenote.gestionnairedenote.services.Calcul;

import java.util.ArrayList;
import java.util.List;

public class GeneralMoyenneMetier {

    private double moyenne;
    private List<Double> notes = new ArrayList<>();

    public List<Double> getNotes() {
        return notes;
    }

    public void setNotes(List<Double> notes) {
        this.notes = notes;
    }

    public GeneralMoyenneMetier() {}

    public GeneralMoyenneMetier(List<Notes> doubles) {
    Calcul calcul = new Calcul();
        this.moyenne = calcul.calculMoyenne(doubles, notes);

    }

    public double getMoyenne() {
        return moyenne;
    }

    public void setMoyenne(double moyenne) {
        this.moyenne = moyenne;
    }
}
