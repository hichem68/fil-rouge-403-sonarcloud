package com.gestionnairedenote.gestionnairedenote.coucheMetier;

import com.gestionnairedenote.gestionnairedenote.model.Notes;
import com.gestionnairedenote.gestionnairedenote.services.Calcul;

import java.util.ArrayList;
import java.util.List;

public class MatiereMoyenneMetier {
    public double moyenne;
    private List<Double> notes1 = new ArrayList<>();

    public List<Double> getNotes1() {
        return notes1;
    }

    public void setNotes1(List<Double> notes1) {
        this.notes1 = notes1;
    }

    public MatiereMoyenneMetier() {}

    public MatiereMoyenneMetier(List<Notes> doubles) {
        Calcul calcul = new Calcul();
        this.moyenne = calcul.calculMoyenne(doubles, notes1);

    }

    public double getMoyenne() {
        return moyenne;
    }

    public void setMoyenne(double moyenne) {
        this.moyenne = moyenne;
    }
}
