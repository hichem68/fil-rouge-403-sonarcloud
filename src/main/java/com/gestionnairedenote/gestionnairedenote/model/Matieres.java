package com.gestionnairedenote.gestionnairedenote.model;
import javax.persistence.*;
import java.util.List;

@Entity
public class Matieres {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idMatieres;
    private String name;
    @ManyToMany(mappedBy = "matieres")
    private List<Notes> notes;


    public Matieres() {
    }

    public Matieres(int idMatieres, String name) {
        this.idMatieres = idMatieres;
        this.name = name;
    }

    public int getIdMatieres() {
        return idMatieres;
    }

    public void setIdMatieres(int idMatieres) {
        this.idMatieres = idMatieres;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Notes> getNotes() {
        return notes;
    }

    public void setNotes(List<Notes> notes) {
        this.notes = notes;
    }
}
