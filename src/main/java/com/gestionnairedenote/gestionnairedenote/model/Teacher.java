package com.gestionnairedenote.gestionnairedenote.model;
import javax.persistence.*;
import java.util.List;

@Entity
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int idTeacher;
    public String nom;
    @OneToMany
    public List<Matieres> matieres;

    public Teacher() {

    }

    public Teacher(int idTeacher, String nom) {
        this.idTeacher = idTeacher;
        this.nom = nom;
    }

    public int getIdTeacher() {
        return idTeacher;
    }

    public void setIdTeacher(int idTeacher) {
        this.idTeacher = idTeacher;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setMatieres(List<Matieres> matieres) {
        this.matieres = matieres;
    }
}
