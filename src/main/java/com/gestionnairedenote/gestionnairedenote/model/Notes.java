package com.gestionnairedenote.gestionnairedenote.model;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Notes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idNotes;
    private double valeur;
    private boolean presence ;
    @ManyToMany(mappedBy = "notes")
    @JsonIgnore
    private List<Students> students;
    @ManyToMany
    private List<Matieres> matieres;


    public Notes() {

    }

    public Notes(double valeur) {
        this.valeur = valeur;
    }

    public Notes(int idNotes, double valeur, boolean presence) {
        this.idNotes = idNotes;
        this.valeur = valeur;
        this.presence = presence;
    }

    public Integer getIdNotes() {
        return idNotes;
    }

    public void setIdNotes(int idNotes) {
        this.idNotes = idNotes;
    }

    public double getValeur() {
        return valeur;
    }

    public void setValeur(double valeur) {
        this.valeur = valeur;
    }

    public boolean isPresence() {
        return presence;
    }

    public void setPresence(boolean presence) {
        this.presence = presence;
    }

    public List<Students> getStudents() {
        return students;
    }

    public void setStudents(List<Students> students) {
        this.students = students;
    }

    public List<Matieres> getMatieres() {
        return matieres;
    }

    public void setMatieres(List<Matieres> matieres) {
        this.matieres = matieres;
    }
}
