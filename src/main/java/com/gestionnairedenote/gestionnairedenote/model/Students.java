package com.gestionnairedenote.gestionnairedenote.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Students {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idStudents;
    private int age ;
    private String name;
    private String prenom;
    private double moyenneM;
    private double moyenneG;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Notes> notes;

    public Students(String name) {
        this.name=name;
    }

    public List<Notes> getNotes() {
        return notes;
    }

    public void setNotes(List<Notes> notes) {
        this.notes = notes;
    }

    public Students() {

    }

    public Students(int idStudents, int age, String name, String prenom, Double moyenneG, Double moyenneM) {
        this.idStudents = idStudents;
        this.age = age;
        this.name = name;
        this.prenom = prenom;
        this.moyenneG = moyenneG;
        this.moyenneM = moyenneM;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getIdStudents() {
        return idStudents;
    }

    public void setIdStudents(int idStudents) {
        this.idStudents = idStudents;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Double getmoyenneG() {
        return moyenneG;
    }

    public void setmoyenneG(int moyenneG) {
        this.moyenneG = moyenneG;
    }

    public Double getMoyenneM() {
        return moyenneM;
    }

    public void setMoyenneM(int moyenneM) {
        this.moyenneM = moyenneM;
    }

    @Override
    public String toString() {
        return "Students{" +
                "age=" + age +
                ", idStudents=" + idStudents +
                ", name='" + name + '\'' +
                ", prenom='" + prenom + '\'' +
                ", moyenneG=" + moyenneG +
                ", moyenneM=" + moyenneM +
                '}';
    }
}
