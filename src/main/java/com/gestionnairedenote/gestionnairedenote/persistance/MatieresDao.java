package com.gestionnairedenote.gestionnairedenote.persistance;
import com.gestionnairedenote.gestionnairedenote.model.Matieres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatieresDao extends JpaRepository<Matieres,Integer> {

     Matieres findByidMatieres(int idMatieres);

}
