package com.gestionnairedenote.gestionnairedenote.persistance;
import com.gestionnairedenote.gestionnairedenote.model.Students;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentsDao extends JpaRepository<Students,Integer> {

    Students findByidStudents(int idStudents);


}

