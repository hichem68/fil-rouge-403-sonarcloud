package com.gestionnairedenote.gestionnairedenote.persistance;

import com.gestionnairedenote.gestionnairedenote.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherDao extends JpaRepository<Teacher,Integer> {

     Teacher findByidTeacher(int idTeacher);

}