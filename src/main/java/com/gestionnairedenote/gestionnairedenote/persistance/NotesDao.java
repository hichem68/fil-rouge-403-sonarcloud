package com.gestionnairedenote.gestionnairedenote.persistance;
import com.gestionnairedenote.gestionnairedenote.model.Notes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface NotesDao extends JpaRepository<Notes,Integer> {

    Notes findByIdNotes(Integer idNotes);

    List<Notes> findByStudentsIdStudents(Integer idStudents);

    List<Notes> findByMatieresIdMatieresAndStudentsIdStudents(Integer idMatieres, Integer idStudents);

    List<Notes> findByMatieresIdMatieres(Integer idMatieres);
}

