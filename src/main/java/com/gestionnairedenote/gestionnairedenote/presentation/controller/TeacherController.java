package com.gestionnairedenote.gestionnairedenote.presentation.controller;
import com.gestionnairedenote.gestionnairedenote.model.Teacher;
import com.gestionnairedenote.gestionnairedenote.persistance.TeacherDao;
import com.gestionnairedenote.gestionnairedenote.presentation.exceptions.NotesIntrouvableException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;


@Api(description = " Gestion des Profs")
@RestController
@CrossOrigin(origins = "*")
public class TeacherController {

        @Autowired
        private TeacherDao teacherDao;


        //Récupérer la liste des Teacher
        @ApiOperation(value = "Réecupère la liste des profs")
        @GetMapping(value="/Teacher")
        public List<String> listeTeacher() {
                List<String> valeur = new ArrayList<>();
                List<Teacher> teacher = teacherDao.findAll();
                for (Teacher teacher1 : teacher){
                        valeur.add(teacher1.getNom());
                }
                return valeur;
        }

        //Récupérer un prof par son Id
        @ApiOperation(value = "Réecupère un prof selon son ID")
        @GetMapping(value="/Teacher/{id}")
        public String afficherUnProf(@PathVariable int id) {
                Teacher teacher = teacherDao.findByidTeacher(id);
                if (teacher == null) throw new NotesIntrouvableException(" le prof avec l'id " + id + " est INTROUVABLE. ecran Bleu si je pouvais.");
                return teacher.getNom();
        }

        @ApiOperation(value = "Permet d'ajouter un prof a la liste des prof")
        @PostMapping(value = "/Teacher")
        public ResponseEntity<Void> ajouterProf(@RequestBody Teacher teacher) {
            Teacher teacher1 = teacherDao.save(teacher);
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{i}")
                    .buildAndExpand(teacher1.getIdTeacher())
                    .toUri();
            return ResponseEntity.created(location).build();

        }
}
