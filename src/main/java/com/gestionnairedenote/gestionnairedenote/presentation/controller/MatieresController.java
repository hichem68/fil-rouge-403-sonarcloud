package com.gestionnairedenote.gestionnairedenote.presentation.controller;
import com.gestionnairedenote.gestionnairedenote.model.Matieres;
import com.gestionnairedenote.gestionnairedenote.persistance.MatieresDao;
import com.gestionnairedenote.gestionnairedenote.presentation.exceptions.NotesIntrouvableException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;


@Api(description = " Gestion des Matieres")
@RestController
@CrossOrigin(origins = "*")
public class MatieresController {

    @Autowired
    private MatieresDao matieresDao;


    //Récupérer la liste des matieres
    @ApiOperation(value = "affiche la liste de toutes les matiere")
    @GetMapping(value = "/Matiere")
    public List<String> listeMatieres() {
        List<String> name = new ArrayList<>();
        List<Matieres> matieres = matieresDao.findAll();
        for ( Matieres matieres2 : matieres) {
            name.add(matieres2.getName());
        }
        return name;
    }

    //Récupérer une matiere par son Id
    @ApiOperation(value = "Réecupère une matiere selon son ID")
    @GetMapping(value = "Matiere/{id}")
    public String afficherUneMatiere(@PathVariable int id) {
        Matieres matieres = matieresDao.findByidMatieres(id);
        if (matieres == null) throw new NotesIntrouvableException(" la matiere avec l'id " + id + " est INTROUVABLE. ecran Bleu si je pouvais.");
        return matieres.getName();
    }

    //ajouter une matiere
    @ApiOperation(value = "Permet d'ajouter une matiere a la liste des matiere")
    @PostMapping(value = "/Matiere")
    public ResponseEntity<Void> ajouterMatiere(@RequestBody Matieres matieres) {
        Matieres matieres1 = matieresDao.save(matieres);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{i}")
                .buildAndExpand(matieres1.getIdMatieres())
                .toUri();
        return ResponseEntity.created(location).build();

    }
}
