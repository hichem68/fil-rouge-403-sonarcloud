package com.gestionnairedenote.gestionnairedenote.presentation.controller;
import com.gestionnairedenote.gestionnairedenote.model.Notes;
import com.gestionnairedenote.gestionnairedenote.persistance.NotesDao;
import com.gestionnairedenote.gestionnairedenote.coucheMetier.GeneralMoyenneMetier;
import com.gestionnairedenote.gestionnairedenote.coucheMetier.MatiereMoyenneMetier;
import com.gestionnairedenote.gestionnairedenote.presentation.exceptions.NotesIntrouvableException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;


@Api(" Gestion des Notes")
@RestController
@CrossOrigin(origins = "*")
public class NotesController {

    private final NotesDao notesDao;

    @Autowired
    public NotesController(NotesDao notesDao) {
        this.notesDao = notesDao;
    }

    //Récupérer la liste des notes
    @ApiOperation(value = "Affiche la liste de toutes les notes")
    @GetMapping(value = "/Note")
    public List<Double> listeNote() {
        List<Double> valeur = new ArrayList<>();
        List<Notes> notes = notesDao.findAll();
        for (Notes note1 : notes) {
            valeur.add(note1.getValeur());
        }
        return valeur;
    }


    //Récupérer une note par son Id
    @ApiOperation(value = "Réecupère une note selon son ID")
    @GetMapping(value = "/Note/{id}")
    public double afficherUneNote(@PathVariable int id) {
        Notes notes = notesDao.findByIdNotes(id);
        if (notes == null)
            throw new NotesIntrouvableException(" la note avec l'id " + id + " est INTROUVABLE. ecran Bleu si je pouvais.");
        return notes.getValeur();
    }


    //        //Récupérer la notes par students par idmatiere
    @ApiOperation(value = "Réecupère toutes les notes d'un etudiant  calcul la moyenne general")
    @GetMapping(value = "/Note/moyenne/{idStudents}")

    public GeneralMoyenneMetier affichetoutesNotesDestudentEtSaMoyenneG(@PathVariable int idStudents) {
        List<Notes> notes = notesDao.findByStudentsIdStudents(idStudents);

        return new GeneralMoyenneMetier(notes);
    }

    //    Récupérer la notes par students par idmatiere je dois pouvoir recupéré seulement la moyenne
    @ApiOperation(value = "Réecupère la moyenne general d'un etudiant")
    @GetMapping(value = "/Note/moyenneG/{idStudents}")

    public double affichelaMoyenneG(@PathVariable int idStudents) {
        List<Notes> moyennes = notesDao.findByStudentsIdStudents(idStudents);
        GeneralMoyenneMetier hichem = new GeneralMoyenneMetier(moyennes);
        return hichem.getMoyenne();
    }


    //Récupérer les note d'une matiere par son idMatiere
    @ApiOperation(value = "Réecupère la liste des notes d'une seule matiere ")
    @GetMapping(value = "/Note/matiere/{idMatieres}")
    public List<Double> afficherLesNoteparMatiere(@PathVariable int idMatieres) {
        List<Double> valeur = new ArrayList<>();
        List<Notes> notes = notesDao.findByMatieresIdMatieres(idMatieres);
        for (Notes note1 : notes) {
            valeur.add(note1.getValeur());
        }
        return valeur;
    }


    //Récupérer la notes par students par idmatiere
    @ApiOperation(value = "Réecupère les notes d'un etudiant selon l'ID de la matiere et calcul la moyenne de la matiere")
    @GetMapping(value = "/Note/moyenne/{idMatieres}/{idStudents}")

    public MatiereMoyenneMetier afficherlesNoteStudentParMatiereetSaMoyenneM(@PathVariable int idMatieres, @PathVariable int idStudents) {
        List<Notes> notes1 = notesDao.findByMatieresIdMatieresAndStudentsIdStudents(idMatieres, idStudents);

        return new MatiereMoyenneMetier(notes1);
    }

    //Récupérer la moyenne par students par idmatiere
    @ApiOperation(value = "Réecupère la moyenne dans une matiere donnée d'un etudiant")
    @GetMapping(value = "/Note/moyenneM/{idMatieres}/{idStudents}")

    public double afficherlaMoyenneM(@PathVariable int idMatieres, @PathVariable int idStudents) {
        List<Notes> hichem1 = notesDao.findByMatieresIdMatieresAndStudentsIdStudents(idMatieres, idStudents);

        MatiereMoyenneMetier moyenneMEtudiant = new MatiereMoyenneMetier(hichem1);
        return moyenneMEtudiant.getMoyenne();
    }


    //ajouter une note
    @ApiOperation(value = "Permet d'ajouter une note a la liste des notes")
    @PostMapping(value = "/Note")
    public ResponseEntity<Void> ajouterNotes(@RequestBody Notes notes) {
        Notes notes1 = notesDao.save(notes);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{i}")
                .buildAndExpand(notes1.getIdNotes())
                .toUri();
        return ResponseEntity.created(location).build();

    }

}

