package com.gestionnairedenote.gestionnairedenote.presentation.controller;

import com.gestionnairedenote.gestionnairedenote.model.Notes;
import com.gestionnairedenote.gestionnairedenote.persistance.NotesDao;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AbsenceController {

    @Autowired
    private NotesDao notesDao;

    //Récupérer la liste des presences
    @ApiOperation(value = "Affiche la liste des presences")
    @GetMapping(value="/Presences")
    public List<Boolean> listePresence() {
        List<Boolean> presence = new ArrayList<>();
        List<Notes> notes = notesDao.findAll();
        for (Notes note2 : notes){
            presence.add(note2.isPresence());
        }
        return presence;
    }

}
