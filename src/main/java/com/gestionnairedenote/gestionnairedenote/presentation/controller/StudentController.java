package com.gestionnairedenote.gestionnairedenote.presentation.controller;

import com.gestionnairedenote.gestionnairedenote.model.Students;
import com.gestionnairedenote.gestionnairedenote.persistance.StudentsDao;
import com.gestionnairedenote.gestionnairedenote.presentation.exceptions.NotesIntrouvableException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;


@Api(description = " Gestion des Students")
@RestController
@CrossOrigin(origins = "*")
public class StudentController {

    public StudentController(StudentsDao studentsDao) {
        this.studentsDao = studentsDao;
    }

    @Autowired
    private final StudentsDao studentsDao;


    //Récupérer la liste des students
    @ApiOperation(value = "affiche la liste de tout les etudiant")
    @RequestMapping(value="/Student", method=RequestMethod.GET)
    public List<String> listeStudents() {
        List<String> valeur = new ArrayList<>();
        List<Students> students = studentsDao.findAll();
        for (Students students1 : students){
            valeur.add(students1.getName());
        }
        return valeur;
    }



    //Récupérer un student par son Id
    @ApiOperation(value = "Réecupère un etudiant selon son ID")
    @GetMapping(value="/Student/{id}")
    public String afficherUnStudent(@PathVariable int id) {
        Students students = studentsDao.findByidStudents(id);
        if (students == null) throw new NotesIntrouvableException(" la note avec l'id " + id + " est INTROUVABLE. ecran Bleu si je pouvais.");

        return students.getName();
    }


    @ApiOperation(value = "Permet d'ajouter un etudiant a la liste des etudiant")
    @PostMapping(value = "/Student")
    public ResponseEntity<Void> ajouterStudent(@RequestBody Students students) {
        Students students1 = studentsDao.save(students);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{i}")
                .buildAndExpand(students1.getIdStudents())
                .toUri();
        return ResponseEntity.created(location).build();

    }

}
